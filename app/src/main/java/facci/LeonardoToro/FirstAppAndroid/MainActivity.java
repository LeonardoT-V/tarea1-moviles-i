package facci.LeonardoToro.FirstAppAndroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_cent = (Button) findViewById(R.id.btn_cent);
        btn_cent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertFahren_Cent();
            }
        });

        Button btn_fahren = (Button) findViewById(R.id.btn_fahren);
        btn_fahren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertCent_Fahren();
            }
        });

    }

    public void convertCent_Fahren() {
        EditText fahren = (EditText) findViewById(R.id.conv_fahren);
        TextView res_fahren = (TextView) findViewById(R.id.res_fahren);
        Number resultado = (Float.parseFloat(fahren.getText().toString()) * 1.8) + 32;

        res_fahren.setText(resultado.toString());
        Log.w("fa",  fahren.getText().toString());

    }

    public void convertFahren_Cent() {
        EditText centigrados = (EditText) findViewById(R.id.conv_cent);
        TextView res_cent = (TextView) findViewById(R.id.res_cent);

        Number resultado = (Float.parseFloat(centigrados.getText().toString()) - 32) / 1.8;

        res_cent.setText(resultado.toString());
    }
}